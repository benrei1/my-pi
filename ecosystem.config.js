module.exports = {
  apps : [{
    name      : 'API',
    script    : 'npm',
      args      : 'run start:production',
    env: {
      NODE_ENV: 'development'
    },
    env_production : {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {},
    staging : {
      user : 'pi',
      host : [{
        host : '62.141.150.83',
        port: '2288'
      }],
      ref  : 'origin/master',
      repo : 'git@gitlab.com:benrei1/my-pi.git',
      path : '/my-pi',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
